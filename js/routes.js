angular
    .module('app')
    .config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$breadcrumbProvider', function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $breadcrumbProvider) {

        $urlRouterProvider.otherwise('/login');

        // $ocLazyLoadProvider.config({
        //     // Set to true if you want to see what and when is dynamically loaded
        //     debug: true
        // });

        $breadcrumbProvider.setOptions({
            prefixStateName: 'app.main',
            includeAbstract: true,
            template: '<li class="breadcrumb-item" ng-repeat="step in steps" ng-class="{active: $last}" ng-switch="$last || !!step.abstract"><a ng-switch-when="false" href="{{step.ncyBreadcrumbLink}}">{{step.ncyBreadcrumbLabel}}</a><span ng-switch-when="true">{{step.ncyBreadcrumbLabel}}</span></li>'
        });

        $stateProvider
            .state('app', {
                abstract: true,
                templateUrl: 'views/common/layouts/full.html',
                //page title goes here
                ncyBreadcrumb: {
                    label: 'Root',
                    skip: true
                },
                resolve: {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load CSS files
                        return $ocLazyLoad.load([{
                            serie: true,
                            name: 'Font Awesome',
                            files: ['css/font-awesome.min.css']
                        }, {
                            serie: true,
                            name: 'Simple Line Icons',
                            files: ['css/simple-line-icons.css']
                        }]);
                    }],
                    loadPlugin: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([{
                                serie: true,
                                files: ['js/libs/gauge.min.js']
                            },
                            {
                                serie: true,
                                files: ['js/libs/angular-gauge.js']
                            },
                            {
                                serie: true,
                                files: ['js/libs/angular-toastr.tpls.min.js']
                            },
                        ]);
                    }],

                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load controllers
                        return $ocLazyLoad.load({
                            files: ['js/app.js']
                        });
                    }]

                }
            })


            .state('app.user', {
                url: '/user',
                templateUrl: 'views/user.html',
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ['js/controllers/user.js']
                        });
                    }]
                }
            })


            .state('app.year', {
                url: '/year',
                templateUrl: 'views/year.html',
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ['js/controllers/year.js']
                        });
                    }]
                }
            })

            .state('app.adminHome', {
                url: '/adminHome',
                templateUrl: 'views/adminHome.html',
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ['js/controllers/adminHome.js']
                        });
                    }]
                }
            })

            .state('app.schoolHome', {
                url: '/schoolHome',
                templateUrl: 'views/schoolHome.html',
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ['js/controllers/schoolHome.js']
                        });
                    }]
                }
            })


            .state('app.classHome', {
                url: '/classHome',
                templateUrl: 'views/classHome.html',
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ['js/controllers/classHome.js']
                        });
                    }]
                }
            })

            .state('app.fee', {
                url: '/fee',
                templateUrl: 'views/fee.html',
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ['js/controllers/fee.js']
                        });
                    }]
                }
            })

            .state('app.student', {
                url: '/student',
                templateUrl: 'views/student.html',
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ['js/controllers/student.js']
                        });
                    }]
                }
            })
            .state('app.studentUpdate', {
                url: '/studentUpdate',
                templateUrl: 'views/studentUpdate.html',
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ['js/controllers/studentUpdate.js']
                        });
                    }]
                }
            })
            .state('app.payment', {
                url: '/payment',
                templateUrl: 'views/payment.html',
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ['js/controllers/payment.js']
                        });
                    }]
                }
            })
            .state('app.studentfeeview', {
                url: '/studentfeeview',
                templateUrl: 'views/studentfeeview.html',
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ['js/controllers/studentfeeview.js']
                        });
                    }]
                }
            })
            .state('app.teacher', {
                url: '/teacher',
                templateUrl: 'views/teacher.html',
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ['js/controllers/teacher.js']
                        });
                    }]
                }
            })

            .state('appSimple', {
                abstract: true,
                templateUrl: 'views/common/layouts/simple.html'
            })

            .state('appSimple.login', {
                url: '/login',
                templateUrl: 'views/login.html',
                resolve: {
                    loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([{
                            serie: true,
                            name: 'Font Awesome',
                            files: ['css/font-awesome.min.css']
                        }, {
                            serie: true,
                            name: 'Simple Line Icons',
                            files: ['css/simple-line-icons.css']
                        }]);
                    }],

                    loadPlugin: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([{
                                serie: true,
                                files: ['js/libs/gauge.min.js']
                            },
                            {
                                serie: true,
                                files: ['js/libs/angular-gauge.js']
                            },
                            {
                                serie: true,
                                files: ['js/libs/angular-toastr.tpls.min.js']
                            },
                        ]);
                    }],
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                        // you can lazy load controllers
                        return $ocLazyLoad.load({
                            files: ['js/controllers/login.js']
                        });
                    }]
                }
            })


            // Additional Pages
            // .state('appSimple.login', {
            //     url: '/login',
            //     templateUrl: 'views/pages/login.html'
            // })
            .state('appSimple.register', {
                url: '/register',
                templateUrl: 'views/pages/register.html'
            })
            .state('appSimple.404', {
                url: '/404',
                templateUrl: 'views/pages/404.html'
            })
            .state('appSimple.500', {
                url: '/500',
                templateUrl: 'views/pages/500.html'
            })


    }]);