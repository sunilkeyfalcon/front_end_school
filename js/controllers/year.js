//main.js
angular
    .module('app')
    .controller('yearCtrl', yearCtrl)
      .directive('validYear', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attr, ngModelCtrl) {
                function fromUser(text) {
                    if (text) {
                        var transformedInput = text.replace(/[^-/0-9]/g, '');
                        if (transformedInput !== text) {
                            ngModelCtrl.$setViewValue(transformedInput);
                            ngModelCtrl.$render();
                        }
                        return transformedInput;
                    }
                    return undefined;
                }
                ngModelCtrl.$parsers.push(fromUser);
            }
        };
    })

yearCtrl.$inject = ['$scope', '$timeout', '$localStorage', '$http', 'constant', 'toastr' , '$location' , '$cookieStore'];

function yearCtrl($scope, $timeout, $localStorage, $http, constant, toastr , $location , $cookieStore) {

  if ($cookieStore.get('roleId') != undefined && $localStorage.currentYear != undefined) {

 

    function successToastr(head, message) {
        toastr.success(message, head, {
            closeButton: true,
            progressBar: true,
        });
    }


    function dangerToastr(head, message) {
        toastr.error(message, head, {
            closeButton: true,
            progressBar: true,
        });
    }


    function infoToastr(head, message) {
        toastr.info(message, head, {
            closeButton: true,
            progressBar: true,
        });
    }



    $scope.getYears = function () {
        $http({
                method: 'GET',
                url: constant.CONTEXT_WEBS + constant.GET_YEAR,
                headers: {
                    'Content-Type': 'application/json;'
                },
            })
            .success(function (response, status, headers, config) {
                $scope.years = response.responseData;
            }).error(function (errResponse) {
                dangerToastr('Error', 'Server Error');
            })
    }
    $scope.getYears();


    $scope.yearObj = {};
    $scope.saveYears = function () {
        if ($scope.yearObj.year != undefined && $scope.yearObj.year.length == 9) {
            $http({
                    method: 'POST',
                    url: constant.CONTEXT_WEBS + constant.SAVE_YEAR,
                    headers: {
                        'Content-Type': 'application/json;'
                    },
                    data: $scope.yearObj
                })
                .success(function (response, status, headers, config) {
                    $scope.getYears();
                    $scope.closeADDModalFun();
                }).error(function (errResponse) {
                    dangerToastr('Error', 'Server Error');
                })
        } else {
            dangerToastr('Error', 'Please Enter Valid year');
        }
    }


    $scope.closeADDModalFun = function () {
        $scope.yearObj = {};
        $('#myModal').modal('hide');
    }


    $scope.updateYear = function (data) {
        $scope.yearObj.id = data.id;
        $scope.yearObj.year = data.yearName;
        $('#myModal').modal('show');
    }





    $scope.yearActiveObj = {};
    $scope.activeYear = function ( data ) {
        $scope.yearActiveObj.id = data.id;
        $http({
                method: 'POST',
                url: constant.CONTEXT_WEBS + constant.ACTIVE_YEAR,
                headers: {
                    'Content-Type': 'application/json;'
                },
                data: $scope.yearActiveObj
            })
            .success(function (response, status, headers, config) {
                $scope.getYears();
            }).error(function (errResponse) {
                dangerToastr('Error', 'Server Error');
            })
    }

   } else {
        $location.path("/login");
    }

}