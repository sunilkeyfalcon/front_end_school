//main.js
angular
    .module('app')
    .controller('studentFeeViewCtrl', studentFeeViewCtrl)


studentFeeViewCtrl.$inject = ['$scope', '$rootScope', '$window', '$timeout', '$localStorage', '$http', 'constant', 'toastr', '$location', '$cookieStore'];

function studentFeeViewCtrl($scope, $rootScope, $window, $timeout, $localStorage, $http, constant, toastr, $location, $cookieStore) {


    if ($cookieStore.get('roleId') != undefined && $localStorage.currentYear != undefined) {




        function successToastr(head, message) {
            toastr.success(message, head, {
                closeButton: true,
                progressBar: true,
            });
        }


        function dangerToastr(head, message) {
            toastr.error(message, head, {
                closeButton: true,
                progressBar: true,
            });
        }


        $rootScope.studentFirstName = $localStorage.studentObj.studentInfo.firstName;
        $rootScope.studentLastName = $localStorage.studentObj.studentInfo.lastName;



        $scope.getPayListObj = {};
        $scope.getStudentPaymentList = function () {

            $scope.getPayListObj.standardId = $localStorage.standardObj.id;
            $scope.getPayListObj.yearId = $localStorage.currentYear.id;
            $scope.getPayListObj.studentId = $localStorage.studentObj.id;
            $http({
                    method: 'POST',
                    url: constant.CONTEXT_WEBS + constant.GET_STUDENTES_PAYMENT_LIST,
                    headers: {
                        'Content-Type': 'application/json;'
                    },
                    data: $scope.getPayListObj
                })
                .success(function (response, status, headers, config) {

                    $scope.paymentResponseObj = response.responseData;
                    $scope.paymentList = response.responseData.paymentDetail;

                    $scope.regularFeesList = response.responseData.feeList;
                    $scope.OptionalFeeList = response.responseData.optionalFeeList;

                    $scope.admissionFee = response.responseData.admissionFee;
                }).error(function (errResponse) {
                    dangerToastr('Error', 'Server Error');
                })
        }
        $scope.getStudentPaymentList();





        $scope.goForPayment = function (data) {
            $rootScope.forPayment = data;
            $location.path("/payment")
        }




        $scope.getFeeTypes = function () {
            $http({
                    method: 'GET',
                    url: constant.CONTEXT_WEBS + constant.GET_FEE_TYPES,
                    headers: {
                        'Content-Type': 'application/json;'
                    },
                })
                .success(function (response, status, headers, config) {
                    $scope.feeTypes = response.responseData;
                }).error(function (errResponse) {
                    dangerToastr('Error', 'Server Error');
                })
        }
        $scope.getFeeTypes();

        $scope.getMonthes = function () {
            $http({
                    method: 'GET',
                    url: constant.CONTEXT_WEBS + constant.GET_MONTHES,
                    headers: {
                        'Content-Type': 'application/json;'
                    },
                })
                .success(function (response, status, headers, config) {
                    $scope.monthes = response.responseData;
                }).error(function (errResponse) {
                    dangerToastr('Error', 'Server Error');
                })
        }
        $scope.getMonthes();

        $scope.closeAddFeeModalFun = function () {
            $scope.saveFeeObj = {};
            $('#myModal').modal('hide');
        }


        $scope.feeTypeFun = function () {
            if ($scope.saveFeeObj.feesTypeId == 1) {
                $scope.showMonth = false;
            } else {
                $scope.showMonth = true;
            }
        }



        $scope.saveFeeObj = {};
        $scope.saveFee = function () {

            $scope.saveFeeObj.status = true;
            $scope.saveFeeObj.studentId = $localStorage.studentObj.id;


            if ($scope.saveFeeObj.feesTypeId != undefined && $scope.saveFeeObj.feesName != undefined && $scope.saveFeeObj.feesAmount != undefined &&
                $scope.saveFeeObj.feesTypeId != '' && $scope.saveFeeObj.feesName != '' && $scope.saveFeeObj.feesAmount != '') {

                var isValid = false;

                if ($scope.saveFeeObj.feesTypeId == 1) {
                    $scope.saveFeeObj.forThisMonthOnly = 0;
                    isValid = true;
                } else {
                    if ($scope.selectedMonthId != undefined && $scope.selectedMonthId != 0) {
                        $scope.saveFeeObj.forThisMonthOnly = $scope.selectedMonthId;
                        isValid = true;
                    }
                }
                if (isValid) {
                    $http({
                        method: 'POST',
                        url: constant.CONTEXT_WEBS + constant.OPTIONAL_FEE_REGISTER,
                        headers: {
                            'Content-Type': 'application/json;'
                        },
                        data: $scope.saveFeeObj
                    }).success(function (response, status, headers, config) {
                        $scope.closeAddFeeModalFun();
                        successToastr('Success', 'Fees Successfully Added/updated');
                        $window.location.reload();
                    }).error(function (errResponse) {
                        dangerToastr('Error', 'Server Error');
                    })
                } else {
                    dangerToastr("Error", "All Feilds Are Required")
                }
            } else {
                dangerToastr("Error", "All Feilds Are Required")
            }
        }





    } else {
        $location.path("/login");
    }


}