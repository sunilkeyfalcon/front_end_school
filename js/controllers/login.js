//main.js
angular
    .module('app')
    .controller('loginCtrl', loginCtrl)

loginCtrl.$inject = ['$scope', '$timeout', '$cookieStore', '$location', 'toastr', '$http', 'constant', '$localStorage'];

function loginCtrl($scope, $timeout, $cookieStore, $location, toastr, $http, constant, $localStorage) {


    $scope.loginObj = {};
    $scope.login = function () {
        if ($scope.loginObj.email != undefined && $scope.loginObj.password != undefined) {


            $http({
                method: 'POST',
                url: constant.CONTEXT_WEBS + constant.USER_LOGIN,
                headers: {
                    'Content-Type': 'application/json;'
                },
                data: $scope.loginObj
            }).success(function (response, status, headers, config) {

                if (response.code == 200) {

                    $scope.getBaseUrl();
                    $cookieStore.put('userId', response.responseData.id);
                    $cookieStore.put('roleId', response.responseData.role.id);

                    $localStorage.currentMonth = response.responseData.month;
                    $localStorage.currentYear = response.responseData.year;

                    if ((response.responseData.role.id == 1)) {
                        $location.path("/adminHome");
                    } else if (response.responseData.role.id == 2) {
                        $cookieStore.put('schoolId', response.responseData.schooles.id);

                        $scope.getObj.id = response.responseData.schooles.id;
                        $scope.getSchool();
                    } else if (response.responseData.role.id == 3) {
                        $cookieStore.put('classId', response.responseData.standard.id);

                        $scope.getObj.id = response.responseData.standard.id;
                        $scope.getClass();
                    }


                } else if (response.code == 201) {
                    toastr.info('Invalid Email', 'Info', {
                        closeButton: true,
                        progressBar: true,
                    });
                } else if (response.code == 202) {
                    toastr.info('Invalid Password', 'Info', {
                        closeButton: true,
                        progressBar: true,
                    });
                } else {
                    toastr.error('Server Error', 'Error', {
                        closeButton: true,
                        progressBar: true,
                    });
                }
            }).error(function (errResponse) {
                toastr.error('Server Error', 'Error', {
                    closeButton: true,
                    progressBar: true,
                });
            })
        } else {
            toastr.error('Email and Password Should Require', 'Error', {
                closeButton: true,
                progressBar: true,
            });
        }


    }





    $scope.getObj = {};
    $scope.getSchool = function () {
        $http({
            method: 'POST',
            url: constant.CONTEXT_WEBS + constant.GET_SCHOOL_BY_ID,
            headers: {
                'Content-Type': 'application/json;'
            },
            data: $scope.getObj
        }).success(function (response, status, headers, config) {
            $localStorage.schoolObj = response.responseData[0];
            $location.path("/schoolHome");
        }).error(function (errResponse) {
            dangerToastr('Error', 'Server Error');
        })
    }



    $scope.getObj = {};
    $scope.getClass = function () {
        $http({
            method: 'POST',
            url: constant.CONTEXT_WEBS + constant.GET_STANDARD_BY_ID,
            headers: {
                'Content-Type': 'application/json;'
            },
            data: $scope.getObj
        }).success(function (response, status, headers, config) {
            $localStorage.standardObj = response.responseData[0];
            $location.path("/classHome");
        }).error(function (errResponse) {
            dangerToastr('Error', 'Server Error');
        })
    }



    $scope.getBaseUrl = function () {
        $http({
            method: 'GET',
            url: constant.CONTEXT_WEBS + constant.IMAGE_BASE_URL,
            headers: {
                'Content-Type': 'application/json;'
            }
        }).success(function (response, status, headers, config) {
            $localStorage.imageBaseURL = response.responseData;
        }).error(function (errResponse) {
            dangerToastr('Error', 'Server Error');
        })
    }






}