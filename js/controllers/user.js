//main.js
angular
    .module('app')
    .controller('userCtrl', userCtrl)

userCtrl.$inject = ['$scope', '$timeout', '$localStorage', '$http', 'constant', 'toastr' , '$location' , '$cookieStore'];

function userCtrl($scope, $timeout, $localStorage, $http, constant, toastr ,  $location , $cookieStore) {


   if ($cookieStore.get('roleId') != undefined && $localStorage.currentYear != undefined) {




    $scope.currentPage = 1;
    $scope.pageSize = 5;


    function successToastr(head, message) {
        toastr.success(message, head, {
            closeButton: true,
            progressBar: true,
        });
    }

    function infoToastr(head, message) {
        toastr.info(message, head, {
            closeButton: true,
            progressBar: true,
        });
    }

    function dangerToastr(head, message) {
        toastr.error(message, head, {
            closeButton: true,
            progressBar: true,
        });
    }


    $scope.getAllUser = function () {
        $http({
                method: 'GET',
                url: constant.CONTEXT_WEBS + constant.USER_GET_ALL,
                headers: {
                    'Content-Type': 'application/json;'
                },
            })
            .success(function (response, status, headers, config) {
                $scope.useres = response.responseData;
            }).error(function (errResponse) {
                dangerToastr('Error', 'Server Error');
            })
    }
    $scope.getAllUser();



    $scope.getAllRoles = function () {
        $http({
                method: 'GET',
                url: constant.CONTEXT_WEBS + constant.USER_GET_ROLES,
                headers: {
                    'Content-Type': 'application/json;'
                },
            })
            .success(function (response, status, headers, config) {
                $scope.roles = response.responseData;
            }).error(function (errResponse) {
                dangerToastr('Error', 'Server Error');
            })
    }
    $scope.getAllRoles();



    $scope.getSchoolList = function () {
        $http({
            method: 'GET',
            url: constant.CONTEXT_WEBS + constant.GET_SCHOOLS,
            headers: {
                'Content-Type': 'application/json;'
            },
        }).success(function (response, status, headers, config) {
            $scope.schoolList = response.responseData;
        }).error(function (errResponse) {
            dangerToastr('Error', 'Server Error');
        })
    }
    $scope.getSchoolList();


    $scope.getStandardObj = {};
    $scope.getStandardBySchool = function (selectedSchoolId) {
        $scope.getStandardObj.id = selectedSchoolId;
        $http({
            method: 'POST',
            url: constant.CONTEXT_WEBS + constant.GET_STANDARD_BY_SCHOOL,
            headers: {
                'Content-Type': 'application/json;'
            },
            data: $scope.getStandardObj
        }).success(function (response, status, headers, config) {
            $scope.standardList = response.responseData;
        }).error(function (errResponse) {
            dangerToastr('Error', 'Server Error');
        })
    }
    $scope.getStandardBySchool();


    function validationFun() {
        if ($scope.saveUserObj.email != undefined && $scope.saveUserObj.password != undefined &&
            $scope.saveUserObj.email.length >= 1 && $scope.saveUserObj.password.length >= 1 &&
            $scope.saveUserObj.roleId != undefined) {
            if ($scope.saveUserObj.roleId == 1) {
                return true;
            } else if ($scope.saveUserObj.roleId == 2) {
                if ($scope.saveUserObj.schoolesId != undefined && $scope.saveUserObj.schoolesId != '') {
                    return true;
                }
                infoToastr('Info', 'Please Select School');
                return false;
            } else if ($scope.saveUserObj.roleId == 3) {
                if ($scope.saveUserObj.standardId != undefined && $scope.saveUserObj.standardId != '') {
                    return true;
                }
                infoToastr('Info', 'Please Select Class');
                return false;
            }
            return true;
        }
        infoToastr('Info', 'Email, Password and Role Should Require');
        return false;
    }




    $scope.saveUserObj = {};
    $scope.saveUser = function () {

        if (validationFun()) {
            $http({
                method: 'POST',
                url: constant.CONTEXT_WEBS + constant.USER_SAVE,
                headers: {
                    'Content-Type': 'application/json;'
                },
                data: $scope.saveUserObj
            }).success(function (response, status, headers, config) {
                $scope.schoolShow = false;
                $scope.classShow = false;
                $scope.closeSaveModalFun();
                $scope.getAllUser();
            }).error(function (errResponse) {
                dangerToastr('Error', 'Server Error');
            })
        }
    }


    $scope.updateUserObj = {};
    $scope.updateUserFun = function (data) {
        $scope.updateUserObj.id = data.id;
        $('#updateModel').modal('show');

    }


    $scope.deleteObj = {};
    $scope.deleteUserFun = function (data) {
        $scope.deleteObj.id = data.id;
        $http({
            method: 'POST',
            url: constant.CONTEXT_WEBS + constant.USER_DELETE,
            headers: {
                'Content-Type': 'application/json;'
            },
            data: $scope.deleteObj
        }).success(function (response, status, headers, config) {
            $scope.getAllUser();
            successToastr('success', 'user successfully delete')
        }).error(function (errResponse) {
            dangerToastr('Error', 'Server Error');
        })
    }



    $scope.updateSaveUserFun = function () {
        if ($scope.updateUserObj.id != undefined && $scope.updateUserObj.password != undefined &&
            $scope.updateUserObj.password.length >= 1) {
            $http({
                method: 'POST',
                url: constant.CONTEXT_WEBS + constant.USER_UPDATE,
                headers: {
                    'Content-Type': 'application/json;'
                },
                data: $scope.updateUserObj
            }).success(function (response, status, headers, config) {
                $scope.closeUpdateModalFun();
                $scope.getAllUser();
                successToastr('success', 'user successfully updated')
            }).error(function (errResponse) {
                dangerToastr('Error', 'Server Error');
            })
        } else {
            infoToastr('Info', 'Please Enter Password');
        }
    }


    $scope.closeUpdateModalFun = function () {
        $scope.updateUserObj = {};
        $('#updateModel').modal('hide');
    }


    $scope.closeSaveModalFun = function () {
        $scope.saveUserObj = {};
        $('#myModal').modal('hide');
    }



    $scope.roleFun = function (dataId) {
        if (dataId == 1) {
            $scope.schoolShow = false;
            $scope.classShow = false;
        } else if (dataId == 2) {
            $scope.schoolShow = true;
            $scope.classShow = false;
        } else if (dataId == 3) {
            $scope.schoolShow = true;
            $scope.classShow = true;
        }
    }


    } else {
        $location.path("/login");
    }

}