//main.js
angular
    .module('app')
    .controller('schoolHomeCtrl', schoolHomeCtrl)
    .directive('mySchool', function () {
        function link(scope, elem, attrs, ngModel) {
            ngModel.$parsers.push(function (viewValue) {
                var reg = /^[^`~!@#$%\^&*()_+={}|[\]\\:';"<>?,.]*$/;
                // if view values matches regexp, update model value
                if (viewValue.match(reg)) {
                    return viewValue;
                }
                // keep the model value as it is
                var transformedValue = ngModel.$modelValue;
                ngModel.$setViewValue(transformedValue);
                ngModel.$render();
                return transformedValue;
            });
        }
        return {
            restrict: 'A',
            require: 'ngModel',
            link: link
        };
    })

schoolHomeCtrl.$inject = ['$scope', '$timeout', '$location', '$localStorage', '$http', 'constant', 'toastr', '$cookieStore'];

function schoolHomeCtrl($scope, $timeout, $location, $localStorage, $http, constant, toastr, $cookieStore) {

    if ($cookieStore.get('roleId') != undefined && $localStorage.currentYear != undefined) {



        $scope.currentPage = 1;
        $scope.pageSize = 5;
        $scope.imageBaseURL = $localStorage.imageBaseURL;

        function successToastr(head, message) {
            toastr.success(message, head, {
                closeButton: true,
                progressBar: true,
            });
        }


        function dangerToastr(head, message) {
            toastr.error(message, head, {
                closeButton: true,
                progressBar: true,
            });
        }


        if ($localStorage.schoolObj != undefined) {
            $scope.schoolName = $localStorage.schoolObj.schoolName;
        }


        $scope.saveStanObj = {};
        $scope.saveStanObj.imageURl = "";
        $scope.saveStandard = function (classImage) {

            if (classImage != undefined && classImage.data.length >= 100)
                $scope.saveStanObj.imageURl = encodeImage(classImage.data);

            $scope.saveStanObj.schoolId = $localStorage.schoolObj.id;
            $scope.saveStanObj.status = true;

            if ($scope.saveStanObj.className != undefined && $scope.saveStanObj.className != '') {
                $http({
                    method: 'POST',
                    url: constant.CONTEXT_WEBS + constant.STANDRD_REGISTER,
                    headers: {
                        'Content-Type': 'application/json;'
                    },
                    data: $scope.saveStanObj
                }).success(function (response, status, headers, config) {

                    if ($scope.saveStanObj.id == undefined || $scope.saveStanObj.id == null) {
                        $scope.saveAdmissionFee(response)
                    }

                    $scope.getStandardBySchool();
                    $scope.closeModalFun();
                    successToastr('Success', 'Standard Successfully Added');
                }).error(function (errResponse) {
                    dangerToastr('Error', 'Server Error');
                })

            } else {
                dangerToastr("Error", "ALL Fields Are Required");
            }
        }


        $scope.admissionObj = {};
        $scope.saveAdmissionFee = function (data) {
            $scope.admissionObj.feesAmount = 0;
            $scope.admissionObj.feesName = 'admission fee';
            $scope.admissionObj.standardId = data.responseData.id;
            $http({
                method: 'POST',
                url: constant.CONTEXT_WEBS + constant.SAVE_ADMISSION_FEE,
                headers: {
                    'Content-Type': 'application/json;'
                },
                data: $scope.admissionObj
            }).success(function (response, status, headers, config) {}).error(function (errResponse) {
                dangerToastr('Error', 'Server Error');
            })
        }


        $scope.getStandardObj = {};
        $scope.getStandardBySchool = function () {
            $scope.getStandardObj.id = $localStorage.schoolObj.id;
            $http({
                method: 'POST',
                url: constant.CONTEXT_WEBS + constant.GET_STANDARD_BY_SCHOOL,
                headers: {
                    'Content-Type': 'application/json;'
                },
                data: $scope.getStandardObj
            }).success(function (response, status, headers, config) {
                $scope.standardList = response.responseData;
            }).error(function (errResponse) {
                dangerToastr('Error', 'Server Error');
            })
        }
        $scope.getStandardBySchool();



        $scope.getTeacherObj = {};
        $scope.getTeachers = function () {
            $scope.getTeacherObj.id = $localStorage.schoolObj.id;
            $http({
                method: 'POST',
                url: constant.CONTEXT_WEBS + constant.GET_TEACHERS_BY_SCHOOL,
                headers: {
                    'Content-Type': 'application/json;'
                },
                data: $scope.getTeacherObj,
            }).success(function (response, status, headers, config) {
                $scope.teacherList = response.responseData;
            }).error(function (errResponse) {
                dangerToastr('Error', 'Server Error');
            })
        }
        $scope.getTeachers();




        $scope.closeModalFun = function () {
            $scope.saveStanObj = {};
            $('#myModal').modal('hide');
        }


        $scope.updateStandard = function (data) {
            $scope.saveStanObj = {};
            $scope.saveStanObj.id = data.id;
            $scope.saveStanObj.className = data.className;
            if (data.teachers != null)
                $scope.saveStanObj.teacherId = data.teachers.id;
            $scope.saveStanObj.status = data.status;
            $scope.saveStanObj.schoolId = data.schooles.id;
            $scope.saveStanObj.imageURl = data.imageURl;
            $('#myModal').modal('show');
        }


        $scope.teacherLoc = function () {
            $location.path("/teacher");
        }




        $scope.schoolView = function (data) {
            $localStorage.standardObj = data;
            $location.path('/classHome');
        }


        function encodeImage(srcData) {
            var array = [];
            array = srcData.split(",");
            return array[1];
        }







    } else {
        $location.path("/login");
    }


}