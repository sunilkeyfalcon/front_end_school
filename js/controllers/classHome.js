//main.js
angular
    .module('app')
    .controller('classHomeCtrl', classHomeCtrl)


classHomeCtrl.$inject = ['$scope', '$rootScope', '$timeout', '$localStorage', '$http', 'constant', 'toastr', '$location', '$cookieStore'];

function classHomeCtrl($scope, $rootScope, $timeout, $localStorage, $http, constant, toastr, $location, $cookieStore) {



    if ($cookieStore.get('roleId') != undefined && $localStorage.currentYear != undefined) {


        $scope.currentPage = 1;
        $scope.pageSize = 5;
        $scope.imageBaseURL = $localStorage.imageBaseURL;


        $scope.teacherHide = true;
        if ($cookieStore.get('roleId') == 3) {
            $scope.teacherHide = false;
        }





        function successToastr(head, message) {
            toastr.success(message, head, {
                closeButton: true,
                progressBar: true,
            });
        }


        function dangerToastr(head, message) {
            toastr.error(message, head, {
                closeButton: true,
                progressBar: true,
            });
        }

        $scope.standardObject = $localStorage.standardObj;

        if ($localStorage.standardObj != undefined) {
            $scope.className = $localStorage.standardObj.className;
        }


        $scope.getStudentObj = {};
        $scope.getStudents = function () {
            $scope.getStudentObj.standardId = $localStorage.standardObj.id;
            $scope.getStudentObj.yearId = $localStorage.currentYear.id;
            $http({
                method: 'POST',
                url: constant.CONTEXT_WEBS + constant.GET_STUDENTES_LIST,
                headers: {
                    'Content-Type': 'application/json;'
                },
                data: $scope.getStudentObj
            }).success(function (response, status, headers, config) {
                $scope.studentList = response.responseData;
            }).error(function (errResponse) {
                dangerToastr('Error', 'Server Error');
            })
        }
        $scope.getStudents();




        $scope.addStudentLoc = function () {
            $rootScope.selectedSudent = undefined;
            $location.path("/student");
        }


        $scope.addFeeLoc = function () {
            $location.path("/fee");
        }



        $scope.updateStudentInfo = function (data) {
            $rootScope.selectedSudent = data;
            $location.path("/student");
        }


        $scope.studentView = function (data) {
            document.getElementById('modelId1').click();
            $scope.studentObj = data.studentInfo;
        }


        $scope.paymentLocation = function (data) {
            $localStorage.studentObj = data;
            $location.path("/studentfeeview");
        }





    } else {
        $location.path("/login");
    }


}