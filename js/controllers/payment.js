//main.js
angular
    .module('app')
    .controller('paymentCtrl', paymentCtrl)


paymentCtrl.$inject = ['$scope', '$rootScope', '$window', '$timeout', '$localStorage', '$http', 'constant', 'toastr', '$location', '$cookieStore'];

function paymentCtrl($scope, $rootScope, $window, $timeout, $localStorage, $http, constant, toastr, $location, $cookieStore) {


    if ($cookieStore.get('roleId') != undefined && $localStorage.currentYear != undefined) {


        if ($rootScope.forPayment != undefined) {


            function successToastr(head, message) {
                toastr.success(message, head, {
                    closeButton: true,
                    progressBar: true,
                });
            }


            function dangerToastr(head, message) {
                toastr.error(message, head, {
                    closeButton: true,
                    progressBar: true,
                });
            }


            $scope.regularFeesList = $rootScope.forPayment.fees;
            $scope.OptionalFeeList = $rootScope.forPayment.optionalFees;
            $scope.admissionFee = $rootScope.forPayment.admissionFee;
            $rootScope.className = $localStorage.standardObj.className;


            $scope.closeAddFeeModalFun = function () {
                $scope.saveFeeObj = {};
                $('#myModal').modal('hide');
            }



            $scope.getTotal = function (data) {
                var total = 0;
                angular.forEach(data, function (el) {
                    total += el.feesAmount;
                });
                return total;
            };



            var feesIds = [];
            var optionalFees = [];

            $scope.feeSelected = function (data) {
                feesIds.push(data);
            }


            $scope.optionalFeeSelected = function (data) {
                optionalFees.push(data);
            }




            $scope.getSumForPayed = function () {
                var total = 0;
                for (var i = 0; i < feesIds.length; i++) {
                    for (var j = 0; j < $scope.regularFeesList.length; j++) {
                        if (feesIds[i] == $scope.regularFeesList[j].feeId) {
                            total = total + $scope.regularFeesList[j].feesAmount;
                        }
                    }
                }

                for (var i = 0; i < optionalFees.length; i++) {
                    for (var j = 0; j < $scope.OptionalFeeList.length; j++) {
                        if (optionalFees[i] == $scope.OptionalFeeList[j].feeId) {
                            total = total + $scope.OptionalFeeList[j].feesAmount;
                        }
                    }
                }
                return total;
            }


            $scope.savePaymentObj = {};
            $scope.PaymentSaveFun = function () {

                if (feesIds.length >= 1 || optionalFees.length >= 1) {

                    $scope.savePaymentObj.monthId = $rootScope.forPayment.month.id;
                    $scope.savePaymentObj.studentId = $localStorage.studentObj.id;
                    if (feesIds.length >= 1)
                        $scope.savePaymentObj.feesIds = feesIds;
                    if (optionalFees.length >= 1)
                        $scope.savePaymentObj.optionalFees = optionalFees;

                    $scope.savePaymentObj.status = true;
                    $scope.savePaymentObj.paymentTotal = $scope.getSumForPayed();
                    $scope.savePaymentObj.yearId = $rootScope.forPayment.year.id;

                    $http({
                            method: 'POST',
                            url: constant.CONTEXT_WEBS + constant.PAYMENT_STUDENT_SAVE,
                            headers: {
                                'Content-Type': 'application/json;'
                            },
                            data: $scope.savePaymentObj
                        })
                        .success(function (response, status, headers, config) {
                            console.log(response)
                            if (response.code == 200) {
                                $window.location.reload();
                                successToastr("Success", "Successfully Payed")
                            } else {
                                dangerToastr('Error', 'Server Error');
                            }
                        }).error(function (errResponse) {
                            dangerToastr('Error', 'Server Error');
                        })
                } else {
                    dangerToastr('Error', 'No , fee for payment');
                }
            }



            $scope.updateAdmissionFee = function (data) {
                if (data != undefined) {
                    $scope.admissionObj = {};
                    $scope.admissionObj.id = data;
                    $http({
                            method: 'POST',
                            url: constant.CONTEXT_WEBS + constant.UPDATE_ADMISSION_FEE,
                            headers: {
                                'Content-Type': 'application/json;'
                            },
                            data: $scope.admissionObj
                        })
                        .success(function (response, status, headers, config) {
                            $rootScope.forPayment.admissionFee.paymentSataus = true;
                        }).error(function (errResponse) {
                            dangerToastr('Error', 'Server Error');
                        })
                }
            }



            $scope.cancelPayment = function () {
                $location.path("/studentfeeview");
            }





        } else {
            $location.path("/studentfeeview")
        }

    } else {
        $location.path("/login");
    }


}