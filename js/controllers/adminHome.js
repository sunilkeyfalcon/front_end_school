//main.js
angular
    .module('app')
    .controller('adminHomeCtrl', adminHomeCtrl)


adminHomeCtrl.$inject = ['$scope', '$timeout', '$location', '$localStorage', '$http', 'constant', 'toastr', '$cookieStore'];

function adminHomeCtrl($scope, $timeout, $location, $localStorage, $http, constant, toastr, $cookieStore) {


    if ($cookieStore.get('roleId') != undefined && $localStorage.currentYear != undefined) {




    $scope.currentPage = 1;
    $scope.pageSize = 5;
    $scope.imageBaseURL = $localStorage.imageBaseURL;

    function successToastr(head, message) {
        toastr.success(message, head, {
            closeButton: true,
            progressBar: true,
        });
    }

    function dangerToastr(head, message) {
        toastr.error(message, head, {
            closeButton: true,
            progressBar: true,
        });
    }


    $scope.closeModalFun = function () {
        $scope.saveObj = {};
        $('#myModal').modal('hide');
    }


    function encodeImage(srcData) {
        var array = [];
        array = srcData.split(",");
        return array[1];
    }


    $scope.saveObj = {};
    $scope.saveObj.imageURL = '';
    $scope.saveSchool = function (schoolImage) {

        if (schoolImage != undefined && schoolImage.data.length >= 100)
            $scope.saveObj.imageURL = encodeImage(schoolImage.data);

        if ($scope.saveObj.schoolName != undefined && $scope.saveObj.city != undefined && $scope.saveObj.address != undefined &&
            $scope.saveObj.schoolName != '' && $scope.saveObj.city != '' && $scope.saveObj.address != '') {
            $http({
                method: 'POST',
                url: constant.CONTEXT_WEBS + constant.SCHOOL_REGISTER,
                headers: {
                    'Content-Type': 'application/json;'
                },
                data: $scope.saveObj
            }).success(function (response, status, headers, config) {
                $scope.getSchoolList();
                $scope.closeModalFun();
                successToastr('Success', 'School Successfully Added');
            }).error(function (errResponse) {
                dangerToastr('Error', 'Server Error');
            })
        } else {
            dangerToastr('Error', 'ALL Fields Are Required');
        }
    }



    $scope.getSchoolList = function () {
        $http({
            method: 'GET',
            url: constant.CONTEXT_WEBS + constant.GET_SCHOOLS,
            headers: {
                'Content-Type': 'application/json;'
            },
        }).success(function (response, status, headers, config) {
            $scope.schoolList = response.responseData;
        }).error(function (errResponse) {
            dangerToastr('Error', 'Server Error');
        })
    }
    $scope.getSchoolList();



    $scope.updateSchool = function (data) {
        $scope.saveObj.id = data.id;
        $scope.saveObj.schoolName = data.schoolName;
        $scope.saveObj.address = data.address;
        $scope.saveObj.imageURL = data.imageURL;
        $scope.saveObj.city = data.city;
        $('#myModal').modal('show');
    }



    $scope.adminView = function (data) {
        $localStorage.schoolObj = data;
        $location.path('/schoolHome');
    }



    $scope.adminEdit = function (data) {
        $scope.updateObj = data;
        document.getElementById('modelId1').click();
    }



    } else {
        $location.path("/login");
    }


}