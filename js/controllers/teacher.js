//main.js
angular
    .module('app')
    .controller('teacherCtrl', teacherCtrl)
    .directive("formatDate", function () {
        return {
            require: 'ngModel',
            link: function (scope, elem, attr, modelCtrl) {
                modelCtrl.$formatters.push(function (modelValue) {
                    if (modelValue) {
                        return new Date(modelValue);
                    } else {
                        return null;
                    }
                });
            }
        };
    });


teacherCtrl.$inject = ['$scope', '$timeout', '$location', '$localStorage', '$http', 'constant', 'toastr' ,'$cookieStore' ];

function teacherCtrl($scope, $timeout, $location, $localStorage, $http, constant, toastr , $cookieStore) {



   if ($cookieStore.get('roleId') != undefined && $localStorage.currentYear != undefined) {


        var dateObj = new Date();
        var requiredDate = dateObj.setMonth(dateObj.getMonth() - 120);
        $scope.maxDate = new Date(requiredDate).toISOString().slice(0, 10);


    $scope.imageBaseURL = $localStorage.imageBaseURL;

    function successToastr(head, message) {
        toastr.success(message, head, {
            closeButton: true,
            progressBar: true,
        });
    }


    function dangerToastr(head, message) {
        toastr.error(message, head, {
            closeButton: true,
            progressBar: true,
        });
    }


    $scope.getGenders = function () {
        $http({
            method: 'GET',
            url: constant.CONTEXT_WEBS + constant.GET_GENDER,
            headers: {
                'Content-Type': 'application/json;'
            },
        }).success(function (response, status, headers, config) {
            $scope.genders = response.responseData;
        }).error(function (errResponse) {
            dangerToastr('Error', 'Server Error');
        })
    }
    $scope.getGenders();

    $scope.getPrefix = function () {
        $http({
            method: 'GET',
            url: constant.CONTEXT_WEBS + constant.GET_NAME_PREFIX,
            headers: {
                'Content-Type': 'application/json;'
            },
        }).success(function (response, status, headers, config) {
            $scope.prefixes = response.responseData;
        }).error(function (errResponse) {
            dangerToastr('Error', 'Server Error');
        })
    }
    $scope.getPrefix();


    $scope.getTeacherObj = {};
    $scope.getTeachers = function () {
        $scope.getTeacherObj.id = $localStorage.schoolObj.id;
        $http({
            method: 'POST',
            url: constant.CONTEXT_WEBS + constant.GET_TEACHERS_BY_SCHOOL,
            headers: {
                'Content-Type': 'application/json;'
            },
            data: $scope.getTeacherObj,
        }).success(function (response, status, headers, config) {
            $scope.teacherList = response.responseData;
        }).error(function (errResponse) {
            dangerToastr('Error', 'Server Error');
        })
    }
    $scope.getTeachers();



    $scope.saveObj = {};
    $scope.saveObj.imageURL = '';
    $scope.saveTeacher = function (teacherImage) {

        if (teacherImage != undefined && teacherImage.data.length >= 100)
            $scope.saveObj.imageURL = encodeImage(teacherImage.data);

        $scope.saveObj.schoolId = $localStorage.schoolObj.id;

        if ($scope.saveObj.prefix != undefined && $scope.saveObj.firstName != undefined && $scope.saveObj.lastName != undefined &&
            $scope.saveObj.address != undefined && $scope.saveObj.dob != undefined && $scope.saveObj.gender != undefined &&
            $scope.saveObj.phone1 != undefined && $scope.saveObj.email != undefined &&
            $scope.saveObj.prefix != '' && $scope.saveObj.firstName != '' && $scope.saveObj.lastName != '' &&
            $scope.saveObj.address != '' && $scope.saveObj.dob != '' && $scope.saveObj.gender != '' &&
            $scope.saveObj.phone1 != '' && $scope.saveObj.email != '') {

            $http({
                method: 'POST',
                url: constant.CONTEXT_WEBS + constant.TEACHER_REGISTER,
                headers: {
                    'Content-Type': 'application/json;'
                },
                data: $scope.saveObj
            }).success(function (response, status, headers, config) {
                $scope.getTeachers();
                $scope.closeModalFun();
                successToastr('Success', 'Teacher Successfully Added');
            }).error(function (errResponse) {
                dangerToastr('Error', 'Server Error');
            })
        } else {
            dangerToastr('Error', 'ALL * Fields Are Require');
        }
    }


    $scope.updateTeacher = function (data) {
        $scope.saveObj = {};
        $scope.saveObj.id = data.id;
        $scope.saveObj.infoId = data.teacherInfo.id;
        $scope.saveObj.schoolId = data.schooles.id;
        $scope.saveObj.status = data.status;

        $scope.saveObj.prefix = data.teacherInfo.prefix;
        $scope.saveObj.firstName = data.teacherInfo.firstName;
        $scope.saveObj.lastName = data.teacherInfo.lastName;
        $scope.saveObj.gender = data.teacherInfo.gender;
        $scope.saveObj.imageURL = data.teacherInfo.imageURL;
        $scope.saveObj.address = data.teacherInfo.address;
        $scope.saveObj.dob = data.teacherInfo.dob;
        $scope.saveObj.email = data.teacherInfo.email;
        $scope.saveObj.phone1 = data.teacherInfo.phone1;
        $scope.saveObj.phone2 = data.teacherInfo.phone2;

        $('#myModal').modal('show');
    }



    $scope.closeModalFun = function () {
        $scope.saveObj = {};
        $('#myModal').modal('hide');
    }




    function encodeImage(srcData) {
        var array = [];
        array = srcData.split(",");
        return array[1];
    }


    } else {
        $location.path("/login");
    }

}