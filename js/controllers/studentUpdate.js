//main.js
angular
    .module('app')
    .controller('studentCtrl', studentCtrl)
    .directive("formatDate", function () {
        return {
            require: 'ngModel',
            link: function (scope, elem, attr, modelCtrl) {
                modelCtrl.$formatters.push(function (modelValue) {
                    if (modelValue) {
                        return new Date(modelValue);
                    } else {
                        return null;
                    }
                });
            }
        };
    });


studentCtrl.$inject = ['$scope', '$rootScope', '$timeout', '$localStorage', '$http', 'constant', 'toastr', '$location', '$cookieStore'];

function studentCtrl($scope, $rootScope, $timeout, $localStorage, $http, constant, toastr, $location, $cookieStore) {



    if ($cookieStore.get('roleId') != undefined && $localStorage.currentYear != undefined) {

        if ($rootScope.selectedSudent == undefined && $rootScope.selectedSudent == null) {
            $location.path("/classHome");
        }

        function successToastr(head, message) {
            toastr.success(message, head, {
                closeButton: true,
                progressBar: true,
            });
        }


        function dangerToastr(head, message) {
            toastr.error(message, head, {
                closeButton: true,
                progressBar: true,
            });
        }


        $scope.getGenders = function () {
            $http({
                method: 'GET',
                url: constant.CONTEXT_WEBS + constant.GET_GENDER,
                headers: {
                    'Content-Type': 'application/json;'
                },
            }).success(function (response, status, headers, config) {
                $scope.genders = response.responseData;
            }).error(function (errResponse) {
                dangerToastr('Error', 'Server Error');
            })
        }
        $scope.getGenders();

        $scope.getCountryies = function () {
            $http({
                method: 'GET',
                url: constant.CONTEXT_WEBS + constant.GET_COUNTRYS,
                headers: {
                    'Content-Type': 'application/json;'
                },
            }).success(function (response, status, headers, config) {
                $scope.countryies = response.responseData;
            }).error(function (errResponse) {
                dangerToastr('Error', 'Server Error');
            })
        }
        $scope.getCountryies();

        $scope.getStates = function () {
            $http({
                method: 'GET',
                url: constant.CONTEXT_WEBS + constant.GET_STATES,
                headers: {
                    'Content-Type': 'application/json;'
                },
            }).success(function (response, status, headers, config) {
                $scope.states = response.responseData;
            }).error(function (errResponse) {
                dangerToastr('Error', 'Server Error');
            })
        }
        $scope.getStates();

        $scope.getPrefix = function () {
            $http({
                method: 'GET',
                url: constant.CONTEXT_WEBS + constant.GET_NAME_PREFIX,
                headers: {
                    'Content-Type': 'application/json;'
                },
            }).success(function (response, status, headers, config) {
                $scope.prefixes = response.responseData;
            }).error(function (errResponse) {
                dangerToastr('Error', 'Server Error');
            })
        }
        $scope.getPrefix();


        function encodeImage(srcData) {
            var array = [];
            array = srcData.split(",");
            return array[1];
        }


        $scope.saveObj = $rootScope.selectedSudent.studentInfo;
        $scope.saveStudent = function (studentImage) {

            if (studentImage != undefined && studentImage.data.length >= 100)
                $scope.saveObj.imageURL = encodeImage(studentImage.data);

            if ($scope.saveObj.firstName != undefined && $scope.saveObj.lastName != undefined && $scope.saveObj.prefix != undefined &&
                $scope.saveObj.fatherName != undefined && $scope.saveObj.address != undefined && $scope.saveObj.dob != undefined &&
                $scope.saveObj.gender != undefined && $scope.saveObj.phone1 != undefined && $scope.saveObj.firstName != '' &&
                $scope.saveObj.lastName != '' && $scope.saveObj.prefix != '' && $scope.saveObj.fatherName != '' && $scope.saveObj.address != '' &&
                $scope.saveObj.dob != '' && $scope.saveObj.gender != '' && $scope.saveObj.phone1 != '') {

console.log($rootScope.selectedSudent.studentInfo)
console.log( $scope.saveObj)

                $http({
                    method: 'POST',
                    url: constant.CONTEXT_WEBS + constant.STUDENT_UPDATE,
                    headers: {
                        'Content-Type': 'application/json;'
                    },
                    data: $scope.saveObj
                }).success(function (response, status, headers, config) {
                    successToastr('Success', 'Student Successfully Added');
                    $scope.addStudentCancel();
                }).error(function (errResponse) {
                    dangerToastr('Error', 'Server Error');
                })
            } else {
                dangerToastr('Error', 'All * Fields are required');
            }
        }


        $scope.addStudentCancel = function () {
            $scope.saveObj = {};
            $location.path("/classHome");
        }








    } else {
        $location.path("/login");
    }

}