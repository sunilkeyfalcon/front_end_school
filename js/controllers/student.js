//main.js
angular
    .module('app')
    .controller('studentCtrl', studentCtrl)
    .directive("formatDate", function () {
        return {
            require: 'ngModel',
            link: function (scope, elem, attr, modelCtrl) {
                modelCtrl.$formatters.push(function (modelValue) {
                    if (modelValue) {
                        return new Date(modelValue);
                    } else {
                        return null;
                    }
                });
            }
        };
    });


studentCtrl.$inject = ['$scope', '$rootScope', '$timeout', '$localStorage', '$http', 'constant', 'toastr', '$location', '$cookieStore'];

function studentCtrl($scope, $rootScope, $timeout, $localStorage, $http, constant, toastr, $location, $cookieStore) {



    if ($cookieStore.get('roleId') != undefined && $localStorage.currentYear != undefined) {

        var dateObj = new Date();
        var requiredDate = dateObj.setMonth(dateObj.getMonth() - 24);
        $scope.maxDate = new Date(requiredDate).toISOString().slice(0, 10);


        //   $scope.maxDate = new Date().toISOString().slice(0,10);

        function successToastr(head, message) {
            toastr.success(message, head, {
                closeButton: true,
                progressBar: true,
            });
        }


        function dangerToastr(head, message) {
            toastr.error(message, head, {
                closeButton: true,
                progressBar: true,
            });
        }


        $scope.getGenders = function () {
            $http({
                method: 'GET',
                url: constant.CONTEXT_WEBS + constant.GET_GENDER,
                headers: {
                    'Content-Type': 'application/json;'
                },
            }).success(function (response, status, headers, config) {
                $scope.genders = response.responseData;
            }).error(function (errResponse) {
                dangerToastr('Error', 'Server Error');
            })
        }
        $scope.getGenders();

        $scope.getCountryies = function () {
            $http({
                method: 'GET',
                url: constant.CONTEXT_WEBS + constant.GET_COUNTRYS,
                headers: {
                    'Content-Type': 'application/json;'
                },
            }).success(function (response, status, headers, config) {
                $scope.countryies = response.responseData;
            }).error(function (errResponse) {
                dangerToastr('Error', 'Server Error');
            })
        }
        $scope.getCountryies();

        $scope.getStates = function () {
            $http({
                method: 'GET',
                url: constant.CONTEXT_WEBS + constant.GET_STATES,
                headers: {
                    'Content-Type': 'application/json;'
                },
            }).success(function (response, status, headers, config) {
                $scope.states = response.responseData;
            }).error(function (errResponse) {
                dangerToastr('Error', 'Server Error');
            })
        }
        $scope.getStates();

        $scope.getPrefix = function () {
            $http({
                method: 'GET',
                url: constant.CONTEXT_WEBS + constant.GET_NAME_PREFIX,
                headers: {
                    'Content-Type': 'application/json;'
                },
            }).success(function (response, status, headers, config) {
                $scope.prefixes = response.responseData;
            }).error(function (errResponse) {
                dangerToastr('Error', 'Server Error');
            })
        }
        $scope.getPrefix();


        function encodeImage(srcData) {
            var array = [];
            array = srcData.split(",");
            return array[1];
        }


        $scope.saveObj = {};
        $scope.saveObj.imageURL = '';
        $scope.saveObj.isNewStudent = false;
        $scope.saveStudent = function (studentImage) {

            $scope.saveObj.standardId = $localStorage.standardObj.id;
            $scope.saveObj.status = true;
            $scope.saveObj.yearId = $localStorage.currentYear.id;
            $scope.saveObj.admissionMonth = $localStorage.currentMonth.id;

            if (studentImage != undefined && studentImage.data.length >= 100)
                $scope.saveObj.imageURL = encodeImage(studentImage.data);

            if ($scope.saveObj.firstName != undefined && $scope.saveObj.lastName != undefined && $scope.saveObj.prefix != undefined &&
                $scope.saveObj.fatherName != undefined && $scope.saveObj.address != undefined && $scope.saveObj.dob != undefined &&
                $scope.saveObj.gender != undefined && $scope.saveObj.phone1 != undefined && $scope.saveObj.firstName != '' &&
                $scope.saveObj.lastName != '' && $scope.saveObj.prefix != '' && $scope.saveObj.fatherName != '' && $scope.saveObj.address != '' &&
                $scope.saveObj.dob != '' && $scope.saveObj.gender != '' && $scope.saveObj.phone1 != '') {

                $http({
                    method: 'POST',
                    url: constant.CONTEXT_WEBS + constant.STUDENT_REGISTER,
                    headers: {
                        'Content-Type': 'application/json;'
                    },
                    data: $scope.saveObj
                }).success(function (response, status, headers, config) {
                    successToastr('Success', 'Student Successfully Added');
                    $scope.saveObj = {};
                }).error(function (errResponse) {
                    dangerToastr('Error', 'Server Error');
                })
            } else {
                dangerToastr('Error', 'All * Fields are required');
            }
        }


        $scope.addStudentCancel = function () {
            $location.path("/classHome");
        }


        if ($rootScope.selectedSudent != undefined && $rootScope.selectedSudent != null) {
            $scope.saveObj = $rootScope.selectedSudent.studentInfo;
            delete $scope.saveObj.createdTime;
            delete $scope.saveObj.updateTime;
        }





    } else {
        $location.path("/login");
    }

}