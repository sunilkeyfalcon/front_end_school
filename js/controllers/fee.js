//main.js
angular
    .module('app')
    .controller('feeCtrl', feeCtrl)


feeCtrl.$inject = ['$scope', '$timeout', '$localStorage', '$http', 'constant', 'toastr', '$location', '$cookieStore'];

function feeCtrl($scope, $timeout, $localStorage, $http, constant, toastr, $location, $cookieStore) {


    if ($cookieStore.get('roleId') != undefined && $localStorage.currentYear != undefined) {




        function successToastr(head, message) {
            toastr.success(message, head, {
                closeButton: true,
                progressBar: true,
            });
        }


        function dangerToastr(head, message) {
            toastr.error(message, head, {
                closeButton: true,
                progressBar: true,
            });
        }


        $scope.getFeeTypes = function () {
            $http({
                    method: 'GET',
                    url: constant.CONTEXT_WEBS + constant.GET_FEE_TYPES,
                    headers: {
                        'Content-Type': 'application/json;'
                    },
                })
                .success(function (response, status, headers, config) {
                    $scope.feeTypes = response.responseData;
                }).error(function (errResponse) {
                    dangerToastr('Error', 'Server Error');
                })
        }
        $scope.getFeeTypes();

        $scope.getMonthes = function () {
            $http({
                    method: 'GET',
                    url: constant.CONTEXT_WEBS + constant.GET_MONTHES,
                    headers: {
                        'Content-Type': 'application/json;'
                    },
                })
                .success(function (response, status, headers, config) {
                    $scope.monthes = response.responseData;
                }).error(function (errResponse) {
                    dangerToastr('Error', 'Server Error');
                })
        }
        $scope.getMonthes();


        $scope.getFeeObj = {};
        $scope.getFee = function () {
            $scope.getFeeObj.standardId = $localStorage.standardObj.id;
            $scope.getFeeObj.yearId = $localStorage.currentYear.id;
            $http({
                method: 'POST',
                url: constant.CONTEXT_WEBS + constant.GET_FEE_BY_YEAR_STANDARD,
                headers: {
                    'Content-Type': 'application/json;'
                },
                data: $scope.getFeeObj
            }).success(function (response, status, headers, config) {
                $scope.feeObject = response.responseData;
            }).error(function (errResponse) {
                dangerToastr('Error', 'Server Error');
            })
        }
        $scope.getFee();


        $scope.getAdmissionFeeObj = {};
        $scope.getAdminssionFee = function () {
            $scope.getAdmissionFeeObj.id = $localStorage.standardObj.id;

            $http({
                method: 'POST',
                url: constant.CONTEXT_WEBS + constant.GET_ADMISSION_FEE_BY_CLASS,
                headers: {
                    'Content-Type': 'application/json;'
                },
                data: $scope.getAdmissionFeeObj
            }).success(function (response, status, headers, config) {
                $scope.admissionFee = response.responseData;
            }).error(function (errResponse) {
                dangerToastr('Error', 'Server Error');
            })
        }
        $scope.getAdminssionFee();


        $scope.feeTypeFun = function () {
            if ($scope.saveFeeObj.feesTypeId == 1) {
                $scope.showMonth = false;
            } else {
                $scope.showMonth = true;
            }
        }

        $scope.saveFeeObj = {};
        $scope.saveFee = function () {

            $scope.saveFeeObj.status = true;
            $scope.saveFeeObj.standardId = $localStorage.standardObj.id;
            $scope.saveFeeObj.yearId = $localStorage.currentYear.id;

            if ($scope.saveFeeObj.feesTypeId != undefined && $scope.saveFeeObj.feesName != undefined && $scope.saveFeeObj.feesAmount != undefined &&
                $scope.saveFeeObj.feesTypeId != '' && $scope.saveFeeObj.feesName != '' && $scope.saveFeeObj.feesAmount != '') {

                var isValid = false;

                if ($scope.saveFeeObj.feesTypeId == 1) {
                    $scope.saveFeeObj.forThisMonthOnly = 0;
                    isValid = true;
                } else {
                    if ($scope.selectedMonthId != undefined && $scope.selectedMonthId != 0) {
                        $scope.saveFeeObj.forThisMonthOnly = $scope.selectedMonthId;
                        isValid = true;
                    }
                }
                if (isValid) {
                    $http({
                        method: 'POST',
                        url: constant.CONTEXT_WEBS + constant.FEES_REGISTER,
                        headers: {
                            'Content-Type': 'application/json;'
                        },
                        data: $scope.saveFeeObj
                    }).success(function (response, status, headers, config) {
                        $scope.getFee();
                        $scope.closeAddFeeModalFun();
                        successToastr('Success', 'Fees Successfully Added/updated');
                    }).error(function (errResponse) {
                        dangerToastr('Error', 'Server Error');
                    })
                } else {
                    dangerToastr("Error", "All Feilds Are Required")
                }
            } else {
                dangerToastr("Error", "All Feilds Are Required")
            }
        }


        $scope.closeAddFeeModalFun = function () {
            $scope.saveFeeObj = {};
            $('#myModal').modal('hide');
        }

        $scope.updateFee = function (data) {
            $('#myModal').modal('show');
            $scope.saveFeeObj = {};
            $scope.saveFeeObj.id = data.id;
            $scope.saveFeeObj.feesTypeId = data.feesType.id;
            $scope.saveFeeObj.feesName = data.feesName;
            $scope.saveFeeObj.feesAmount = data.feesAmount;
        }




        $scope.admissionObj = {};
        $scope.saveAdmissionFee = function (data) {

            $http({
                method: 'POST',
                url: constant.CONTEXT_WEBS + constant.SAVE_ADMISSION_FEE,
                headers: {
                    'Content-Type': 'application/json;'
                },
                data: $scope.admissionObj
            }).success(function (response, status, headers, config) {
                $scope.getAdminssionFee();
                $scope.closeAdmissionFeeModalFun ();
            }).error(function (errResponse) {
                dangerToastr('Error', 'Server Error');
            })
        }


        $scope.closeAdmissionFeeModalFun = function () {
            $scope.admissionObj = {};
            $('#admissionModal').modal('hide');
        }

        $scope.updateAdmissionFee = function (data) {
            $('#admissionModal').modal('show');
            $scope.admissionObj.feesAmount = data.feesAmount;
            $scope.admissionObj.feesName = data.feesName;
            $scope.admissionObj.standardId = data.standard.id;
            $scope.admissionObj.id = data.id;
        }










    } else {
        $location.path("/login");
    }

}