angular
    .module('app')
    .constant('constant', {


             CONTEXT_WEBS: 'http://54.89.237.227:8080//school/',
          //   CONTEXT_WEBS: 'http://localhost:8080/school/',
          //  CONTEXT_WEBS: 'http://122.166.212.177:8087/school/',


        USER_LOGIN: 'user/Login',
        USER_GET_ALL: 'user/getAll',
        USER_GET_ROLES: 'user/roles',
        USER_SAVE: 'user/save',
        USER_UPDATE: 'user/update',
        USER_DELETE: 'user/delete',

        SCHOOL_REGISTER: 'school/save',
        TEACHER_REGISTER: 'teacher/save',
        STANDRD_REGISTER: 'standard/save',
        STUDENT_REGISTER: 'student/save',
        STUDENT_UPDATE: 'student/update',

        FEES_REGISTER: 'fee/save',
        OPTIONAL_FEE_REGISTER: 'fee/save/optional/fee',
        PAYMENT_STUDENT_SAVE: 'payment/save',
        GET_FEE_TYPES: 'fee/get/fee/type',
        GET_FEE_BY_YEAR_STANDARD: 'fee/get/fee',

        GET_GENDER: 'utility/gender',
        GET_COUNTRYS: 'utility/country',
        GET_STATES: 'utility/state',
        GET_NAME_PREFIX: 'utility/prefix',
        GET_MONTHES: 'utility/month',

        GET_STUDENTES_LIST: 'student/get/student',
        GET_STUDENTES_PAYMENT_LIST: 'payment/get/payment/list',
        GET_SCHOOLS: 'school/get/schools',
        GET_STANDARD_BY_SCHOOL: 'standard/get/standard/by/school',
        GET_TEACHERS_BY_SCHOOL: 'teacher/get/teacher/by/school',

        GET_SCHOOL_BY_ID: 'school/get/school/byId',
        GET_STANDARD_BY_ID: 'standard/get/standard/by/Id',
        SAVE_ADMISSION_FEE: 'fee/save/admission/fee',
        GET_ADMISSION_FEE_BY_CLASS: 'fee/get/admission/fee',
        UPDATE_ADMISSION_FEE: 'student/save/admissionFee',

        GET_YEAR: 'utility/year',
        SAVE_YEAR: 'utility/year/save',
        ACTIVE_YEAR: 'utility/year/active',

        IMAGE_BASE_URL: 'utility/image/baseurl',

    });